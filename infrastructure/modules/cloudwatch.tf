resource "aws_cloudwatch_log_group" "example_app" {
  name = "ecs/${var.app_name}-${var.env_name}"
}