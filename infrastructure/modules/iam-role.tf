#execution role used by task to connect to ecr repo to pull image/cloudwatch ogs
resource "aws_iam_role" "ecs_execution_role" {
  name = "${var.app_name}-${var.env_name}"
  assume_role_policy = jsonencode({
    "Version" = "2012-10-17",
    Statement = [
      {
        Sid    = "roleForEcsContainer",
        Effect = "Allow",
        Principal = {
          Service = ["ecs-tasks.amazonaws.com"]
        },
        Action = "sts:AssumeRole"
      }
    ]

  })
}

