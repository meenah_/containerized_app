resource "aws_security_group" "greeting_app_lb" {
  name        = "${var.app_name}-${var.env_name}-alb-security-group"
  vpc_id      = aws_vpc.main.id
  description = "Application Load Balancer (ALB) access"

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "greeting_app_ecs_tasks" {
  name        = "${var.app_name}-${var.env_name}-ecs-tasks-security-group"
  vpc_id      = aws_vpc.main.id
  description = "Allow ingress access from the ALB only"

  ingress {
    protocol        = "tcp"
    from_port       = 5000
    to_port         = 5000
    cidr_blocks     = ["0.0.0.0/0"]
    security_groups = [aws_security_group.greeting_app_lb.id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

}