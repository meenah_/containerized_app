
data "terraform_remote_state" "global" {
  backend = "s3"

  config = {
    bucket = "terraform-remote-state-k"
    key    = "${var.env_name}/terraform.tfstate"
    region = var.region
  }
}

data "aws_iam_policy_document" "ecs_execution_policy" {

  statement {
    sid = "allowAccessToEcr"
    actions = [
      "ecr:GetAuthorizationToken",
      "ecr:BatchCheckLayerAvailability",
      "ecr:GetDownloadUrlForLayer",
      "ecr:BatchGetImage"
    ]

    resources = ["*"]
  }

  statement {
    sid = "allowAccessToCloudwatch"
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = ["*"]
  }

  statement {
    sid = "iamPassRole"

    actions = [
      "iam:PassRole",
    ]

    resources = ["*"]

    condition {
      test     = "StringLike"
      variable = "iam:PassedToService"

      values = [
        "ecs-tasks.amazonaws.com"
      ]
    }
  }

}