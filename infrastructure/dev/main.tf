module "greeting_app" {
  source = "../modules"

  region               = local.region
  app_name             = local.app_name
  env_name             = local.env_name
  cidr_block           = local.cidr_block
  availability_zones   = local.availability_zones
  public_subnets_cidr  = local.public_subnets_cidr
  private_subnets_cidr = local.private_subnets_cidr
  tags                 = local.tags
}
